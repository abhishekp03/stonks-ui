import React, { useState } from 'react';
import { Button, Form, Segment } from 'semantic-ui-react';
import APIGateway from '../utils/APIGateway';

const Profile = () => {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const onSubmit = () => {
        const profileData = { userId: "123", firstName, lastName };
        APIGateway.put('/profile', profileData)
            .then((response) => console.log(response.data.body))
            .catch((err) => console.log(err));
    }

    return (
        <Segment placeholder>
            <h3>Complete Profile</h3>
            <Form onSubmit={onSubmit}>
                <Form.Field>
                    <label>First Name</label>
                    <input
                        placeholder='John'
                        value={firstName}
                        onChange={event => setFirstName(event.target.value)}
                    />
                </Form.Field>

                <Form.Field>
                    <label>Last Name</label>
                    <input
                        placeholder='Smith'
                        value={lastName}
                        onChange={event => setLastName(event.target.value)}
                    />
                </Form.Field>

                <Button type='submit' content='Save' icon='save' color='green' />
            </Form >
        </Segment>
    );
}

export default Profile;