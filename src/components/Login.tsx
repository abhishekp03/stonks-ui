import React, { useState, useContext } from 'react';
import { Button, Form, Grid, Segment } from 'semantic-ui-react';
import { AccountContext } from '../utils/Accounts';

const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const { authenticate } = useContext(AccountContext);

    const onSubmit = (event: any) => {
        event.preventDefault();

        authenticate(email, password)
            .then((data: any) => {
                console.log('Logged in!', data);
            })
            .catch((err: any) => {
                console.error('Failed to login!', err);
            })
    };

    return (
        <Segment placeholder>
            <Grid columns={2} relaxed='very' stackable>
                <Grid.Column>
                    <Form onSubmit={onSubmit}>
                        <Form.Input
                            icon='user'
                            iconPosition='left'
                            label='Username'
                            placeholder='Username'
                            value={email}
                            onChange={event => setEmail(event.target.value)}
                        />
                        <Form.Input
                            icon='lock'
                            iconPosition='left'
                            label='Password'
                            type='password'
                            placeholder='Password'
                            value={password}
                            onChange={event => setPassword(event.target.value)}
                        />

                        <Button content='Login' primary />
                    </Form>
                </Grid.Column>

                <Grid.Column verticalAlign='middle'>
                    <Button content='Sign up' icon='signup' size='big' color='green' />
                </Grid.Column>
            </Grid>

        </Segment>
    );
}

export default Login;