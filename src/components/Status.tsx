import React, { useState, useContext, useEffect } from 'react';
import { AccountContext } from '../utils/Accounts';

const Status = () => {
    const [status, setStatus] = useState(false);

    const { getSession, logout } = useContext(AccountContext);

    useEffect(() => {
        getSession()
            .then((session: any) => {
                console.log('Session:', session);
                setStatus(true);
            })
    });

    return (
        <div>
            {status ? (
                <div>
                    You are logged in.&nbsp;
                    <button onClick={logout}>Logout</button>
                </div>
            ) : 'Please login below.'}
        </div>
    );
};

export default Status;