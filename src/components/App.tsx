import React from 'react';
import Login from './Login';
import Signup from './Signup';
import Status from './Status';
import Profile from './Profile';
import { Account } from '../utils/Accounts';

const App = () => {
    return (
        <Account>
            <Signup />
            <Login />
            <Status />
            <Profile />
        </Account>
    );
}

export default App;