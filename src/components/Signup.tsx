import React, { useState } from 'react';
import { Button, Checkbox, Form, Segment } from 'semantic-ui-react';
import CognitoUserPool from '../utils/CognitoUserPool';

const Signup = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const onSubmit = (event: any) => {
        event.preventDefault();

        CognitoUserPool.signUp(email, password, [], [], (err, data) => {
            if (err) console.error(err);
            console.log(data);
        });
    };

    return (
        <Segment placeholder>
            <Form onSubmit={onSubmit}>
                <Form.Field>
                    <label>Email</label>
                    <input
                        placeholder='Email'
                        value={email}
                        onChange={event => setEmail(event.target.value)}
                    />
                </Form.Field>

                <Form.Field>
                    <label>Password</label>
                    <Form.Input
                        type='password'
                        placeholder='Password'
                        value={password}
                        onChange={event => setPassword(event.target.value)}
                    />
                </Form.Field>

                <Form.Field>
                    <label>Re-enter Password</label>
                    <Form.Input
                        type='password'
                        placeholder='Re-enter Password'
                    />
                </Form.Field>

                <Form.Field>
                    <Checkbox label='I agree to the Terms and Conditions' />
                </Form.Field>
                <Button type='submit' content='Sign up' icon='signup' color='green' />
            </Form >
        </Segment>
    );
}

export default Signup;