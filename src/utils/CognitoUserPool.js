import { CognitoUserPool } from 'amazon-cognito-identity-js';

const poolData = {
  UserPoolId: 'us-east-2_aeq7xZWEW',
  ClientId: '5ct6mq43t69htiulq76h8kt572'
};

export default new CognitoUserPool(poolData);